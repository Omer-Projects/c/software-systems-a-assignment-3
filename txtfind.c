#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Max size that word can be
 */
#define WORD_MAX_SIZE 1024

/**
 * @brief Get file content from user into array
 *
 * @param length pointer for output the length of the file
 * @return char* pointer to the file array
 */
char *get_file(int *length)
{
    int fileLength = 0;
    int fileCupacity = 2;
    char *file = (char *)malloc(fileCupacity);

    while (scanf("%c", file + fileLength) != -1)
    {
        fileLength++;

        if (fileLength + 1 == fileCupacity)
        {
            fileCupacity *= 2;
            char *temp = (char *)malloc(fileCupacity);
            memcpy((void *)temp, (void *)file, fileLength);

            free(file);
            file = temp;
        }
    }

    *(file + fileLength) = '\0';

    *length = fileLength;

    return file;
}

/**
 * @brief Print all the lines in file content that have the word
 *
 * @param word a word to fine
 * @param wordLength the length of the wordLength
 * @param file the file content
 * @param fileLength the length of the file content
 */
void print_lines(char *word, int wordLength, char *file, int fileLength)
{
    int startLineIndex = 0;

    // over lines
    while (startLineIndex < fileLength)
    {
        if (*(file + startLineIndex) == '\n')
        {
            startLineIndex++;
        }

        if (startLineIndex < fileLength)
        {
            int inLine = 0;

            // over chars
            int j = startLineIndex;
            while (*(file + j) != '\n' && *(file + j) != '\0' && inLine == 0)
            {
                int i = 0;
                while (i < wordLength && *(file + j + i) == *(word + i))
                {
                    i++;
                }

                if (i == wordLength)
                {
                    inLine = 1;
                }
                else
                {
                    j++;
                }
            }

            // the word found
            if (inLine == 1)
            {
                while (*(file + j) != '\n' && *(file + j) != '\0')
                {
                    j++;
                }

                for (size_t i = startLineIndex; i < j - 1; i++)
                {
                    printf("%c", *(file + i));
                }
                printf("\n");
            }

            startLineIndex = j;
        }
    }
}

/**
 * @brief Check if tv is end of a word
 *
 * @param tv the tv to check
 * @return int 1 - if is end of a word, 0 - elsewhere
 */
int is_end_of_word(char tv)
{
    if (tv == ' ' || tv == '\n' || tv == '\0')
    {
        return 1;
    }
    return 0;
}

/**
 * @brief Print all the words in file content that same to word or has one char more then the word
 *
 * @param word a word to fine
 * @param wordLength the length of the wordLength
 * @param file the file content
 * @param fileLength the length of the file content
 */
void print_similar_words(char *word, int wordLength, char *file, int fileLength)
{
    int wordStart = 0;
    // over the words
    while (wordStart + wordLength < fileLength)
    {
        int countOfExtraChars = 0;
        int i = 0;
        while (countOfExtraChars < 2 && i - countOfExtraChars < wordLength && !is_end_of_word(*(file + wordStart + i)))
        {
            if (*(file + wordStart + i) != *(word + i - countOfExtraChars))
            {
                countOfExtraChars++;
            }
            i++;
        }

        int endOfWord = 0;
        if (is_end_of_word(*(file + wordStart + i)))
        {
            endOfWord = 1;
        }
        else if (countOfExtraChars == 0 && is_end_of_word(*(file + wordStart + i + 1)))
        {
            endOfWord = 1;
            countOfExtraChars++;
            i++;
        }

        if (i - countOfExtraChars == wordLength && countOfExtraChars < 2 && endOfWord == 1)
        {
            // needed to print
            for (int j = 0; j < wordLength + countOfExtraChars; j++)
            {
                printf("%c", *(file + wordStart + j));
            }
            printf("\n");
        }
        else
        {
            // not needed to print
            while (!is_end_of_word(*(file + wordStart + i)))
            {
                i++;
            }
        }

        wordStart += i + 1;
    }
}

/**
 * @brief Entry point of txtfind
 *
 * @return int error code
 */
int main()
{
    // get word and option from the user
    char word[WORD_MAX_SIZE];
    bzero(word, WORD_MAX_SIZE);

    char option;

    scanf("%s %c\n\n", word, &option);

    // get body of file from user
    int fileLength;
    char *file = get_file(&fileLength);

    // dos the request
    switch (option)
    {
    case 'a':
        print_lines(word, strlen(word), file, fileLength);
        break;
    case 'b':
        print_similar_words(word, strlen(word), file, fileLength);
        break;

    default:
        break;
    }

    // cleanup
    free(file);

    return 0;
}
