# Project Path -
PROJECT_PATH=.

# Compiler prefix
CC=gcc -Wall

# clean, fixers and lintters
clean:
	rm -rf *.o isort txtfind

# code units
isort.o: $(PROJECT_PATH)/isort.c
	$(CC) -c "$(PROJECT_PATH)/isort.c" -o isort.o

txtfind.o: $(PROJECT_PATH)/txtfind.c
	$(CC) -c "$(PROJECT_PATH)/txtfind.c" -o txtfind.o

# applications
isort: $(PROJECT_PATH)/isort.o
	$(CC) isort.o -o isort

txtfind: $(PROJECT_PATH)/txtfind.o
	$(CC) txtfind.o -o txtfind

# prod
all: isort txtfind

rebuild: clean all

# testing
test-isort: rebuild
	./isort < "$(PROJECT_PATH)/inputs/sort_input.txt"

test-txtfind-a: rebuild
	./txtfind < "$(PROJECT_PATH)/inputs/find_inputa.txt"

test-txtfind-b: rebuild
	./txtfind < "$(PROJECT_PATH)/inputs/find_inputb.txt"

test: rebuild
	./isort < "$(PROJECT_PATH)/inputs/sort_input.txt"
	./txtfind < "$(PROJECT_PATH)/inputs/find_inputa.txt"
	./txtfind < "$(PROJECT_PATH)/inputs/find_inputb.txt"
