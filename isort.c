#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief shift i elements off array from 0 -> i to 1 -> i + 1
 *
 * @param arr pointer to the start off the array
 * @param i the number of elements to shift
 */
void shift_element(int *arr, int i)
{
    // Only if i is positive
    if (i > 0)
    {
        // shift the elements from the end (i) to the start (arr)
        int *end = arr + i;

        while (arr != end)
        {
            *(end) = *(end - 1);
            end--;
        }
    }
}

/**
 * @brief Insertion Sort
 *
 * @param arr array
 * @param len length of the array
 */
void insertion_sort(int *arr, int len)
{
    // if the array less then two it nod need sorting
    if (len > 1)
    {
        for (int i = 1; i < len; i++)
        {
            int num = *(arr + i);

            int j = 0;
            while (*(arr + j) < num && j < i)
            {
                j++;
            }

            if (j < i)
            {
                shift_element(arr + j, i - j);
                *(arr + j) = num;
            }
        }
    }
}

/**
 * @brief print array of numbers
 *
 * @param arr array of numbers
 * @param length length of the array
 */
void print_array(int *arr, int length)
{
    if (length > 0)
    {
        for (int i = 0; i < length - 1; i++)
        {
            printf("%d,", *(arr + i));
        }
        printf("%d\n", *(arr + length - 1));
    }
}

/**
 * @brief Entry point of isort
 *
 * @return int error code
 */
int main()
{
    // get array of numbers from user
    int length = 0;
    int cupacity = 2;
    int *arr = (int *)malloc(cupacity * sizeof(int));

    while (scanf("%d", arr + length) != -1)
    {
        length++;

        if (length == cupacity)
        {
            cupacity *= 2;
            int *temp = (int *)malloc(cupacity * sizeof(int));
            memcpy((void *)temp, (void *)arr, length * sizeof(int));

            free(arr);
            arr = temp;
        }
    }

    // insertion_sort
    insertion_sort(arr, length);

    // print sorted array
    print_array(arr, length);

    // cleanup
    free(arr);

    return 0;
}
